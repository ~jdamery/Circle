[[!meta title="Note to Cat I"]]
[[!tag cat bearhug]]

Note to Cat: This is a Sink.  

The primary purpose of sinks is to be receptacles for water, not for
cats.

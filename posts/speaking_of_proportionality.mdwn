[[!meta title="Speaking of Proportionality"]]
[[!tag politics ge15 fptp electoral-reform]]

One thing that's been noted about this election is the unusual
disparity in ranges of numbers of votes required to secure seats
between parties.  To some extent this is because the SNP had a
"roaring" good election and won 56 out of 59 seats that they contested
(giving them very little chance to "waste" votes).  On the other side
UKIP contested an awful lot of seats for very little effect; but here
I present this figures in a very slightly easier to read form.  Maybe
later I'll produce an infographic.

For reasons the Speaker, if seeking reelection as Speaker, is
traditionally regarded as neutral and not contested (in fact both UKIP
and Green did contest his seat); so I'm taking my base unit as the
number of votes required to elect one John Bercow - 34,617.  For each
of the parties who won at least one seat, how many Bercows did it take
to elect each MP on average?  These figures have been slightly rounded
for presentation

| Party            | Bercows   |
|:-----------------|:---------:|
| Speaker          |    1      |
| Conservative     |    1      |
| DUP              |    ⅔      |
| Green Party      |  33⅖      |
| Labour           |    1⅙     |
| Liberal Democrat |    8¾     |
| Plaid Cymru      |    1¾     |
| SDLP             |    1      |
| SNP              |    ¾      |
| Sinn Fein        |    1¼     |
| UKIP             |  112⅛     |
| UUP              |    1⅔     |

[//]: # ( | Party            | Bercows | )
[//]: # ( |:-----------------|:-------:| )
[//]: # ( | Speaker          |      1  | )
[//]: # ( | Conservative     |      1  | )
[//]: # ( | DUP              |  0.666  | )
[//]: # ( | Green Party      |  33.44  | )
[//]: # ( | Labour           |  1.164  | )
[//]: # ( | Liberal Democrat |   8.72  | )
[//]: # ( | Plaid Cymry      |   1.75  | )
[//]: # ( | SDLP             |   0.96  | )
[//]: # ( | SNP              |   0.75  | )
[//]: # ( | Sinn Fein        |   1.27  | )
[//]: # ( | UKIP             | 112.12  | )
[//]: # ( | UUP              |   1.66  | )
[//]: # ( Alliance 1.78 )
[//]: # ( TUSC 1.05 )
[//]: # ( Other 6.282 )
In addition the Alliance Party got 1¾ Bercows and no seat, and the TUSC
got just over 1 Bercow and no seat.  Other candidates between them got 6¼
Bercows without any seats. 
